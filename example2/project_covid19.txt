Last	BPH
First	LANL
Center	LANL Biosecurity and Public Health 
Type	Center
Website	https://edge-covid19.edgebioinformatics.org/
ProjectName	Example biological experiment
ProjectTitle	Example biological experiment
Description	Example project for NCBI SRA submission  
Organism	Severe acute respiratory syndrome coronavirus 2
Email	chienchi@lanl.gov
Release	2021-03-07
Resource	{"EDGE-COVID19":"https://edge-covid19.edgebioinformatics.org/"}
